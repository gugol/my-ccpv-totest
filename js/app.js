(function(w) {
/* ======= Model ======= */

w.model = {
    currentCat: null,
    cats: [
        {
            clickCount : 0,
            name : 'Tabby',
            imgSrc : 'img/Basel.jpg'
        },
        {
            clickCount : 0,
            name : 'Tiger',
            imgSrc : 'img/Bill.jpg'
        },
        {
            clickCount : 0,
            name : 'Scaredy',
            imgSrc : 'img/Catman.jpg'
        },
        {
            clickCount : 0,
            name : 'Shadow',
            imgSrc : 'img/Sandy.jpg'
        },
        {
            clickCount : 0,
            name : 'Sleepy',
            imgSrc : 'img/Tomcat.jpg'
        }
    ]
};


/* ======= Octopus ======= */

w.octopus = {

    init: function() {
        // set our current cat to the first one in the list
        w.model.currentCat = w.model.cats[0];

        // tell our views to initialize
        w.catListView.init();
        w.catView.init();
    },

    getCurrentCat: function() {
        return w.model.currentCat;
    },

    getCats: function() {
        return w.model.cats;
    },

    // set the currently-selected cat to the object passed in
    setCurrentCat: function(cat) {
        w.model.currentCat = cat;
    },

    // increments the counter for the currently-selected cat
    incrementCounter: function() {
        w.model.currentCat.clickCount++;
        w.catView.render();
    }
};


/* ======= View ======= */

w.catView = {

    init: function() {
        // store pointers to our DOM elements for easy access later
        this.catElem = document.getElementById('cat');
        this.catNameElem = document.getElementById('cat-name');
        this.catImageElem = document.getElementById('cat-img');
        this.countElem = document.getElementById('cat-count');

        // on click, increment the current cat's counter
        this.catImageElem.addEventListener('click', function(){
            w.octopus.incrementCounter();
        });

        // render this view (update the DOM elements with the right values)
        this.render();
    },

    render: function() {
        // update the DOM elements with values from the current cat
        var currentCat = w.octopus.getCurrentCat();
        this.countElem.textContent = currentCat.clickCount;
        this.catNameElem.textContent = currentCat.name;
        this.catImageElem.src = currentCat.imgSrc;
    }
};

w.catListView = {

    init: function() {
        // store the DOM element for easy access later
        this.catListElem = document.getElementById('cat-list');

        // render this view (update the DOM elements with the right values)
        this.render();
    },

    render: function() {
        var cat, elem, i;
        // get the cats we'll be rendering from the octopus
        var cats = w.octopus.getCats();

        // empty the cat list
        this.catListElem.innerHTML = '';

        // loop over the cats
        for (i = 0; i < cats.length; i++) {
            // this is the cat we're currently looping over
            cat = cats[i];

            // make a new cat list item and set its text
            elem = document.createElement('li');
            elem.textContent = cat.name;

            // on click, setCurrentCat and render the catView
            // (this uses our closure-in-a-loop trick to connect the value
            //  of the cat variable to the click event function)
            elem.addEventListener('click', (function(catCopy) {
                return function() {
                    w.octopus.setCurrentCat(catCopy);
                    catView.render();
                };
            })(cat));

            // finally, add the element to the list
            this.catListElem.appendChild(elem);
        }
    }
};

// make it go!
w.octopus.init();

})(window);

// console.log(this);
// console.log(window);

// console.log(window.model);
// console.log(window.octopus);