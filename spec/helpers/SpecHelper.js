// beforeEach(function () {
//   jasmine.addMatchers({
//     toBePlaying: function () {
//       return {
//         compare: function (actual, expected) {
//           var player = actual;

//           return {
//             pass: player.currentlyPlayingSong === expected && player.isPlaying
//           }
//         }
//       };
//     }
//   });
// });


var jsdom = require("jsdom").jsdom;

const doc = jsdom('<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <title>Cat Clicker</title></head><body> <ul id="cat-list"></ul> <div id="cat"> <h2 id="cat-name"></h2> <div id="cat-count"></div> <img id="cat-img" src="" alt="cute cat"> </div> <script src="js/app.js"></script></body></html>');

const win = doc.defaultView;

if(Object.keys(win).length === 0) {
    // this hapens if contextify, one of jsdom's dependencies doesn't install correctly
    // (it installs different code depending on the OS, so it cannot get checked in.);
    throw "jsdom failed to create a usable environment, try uninstalling and reinstalling it";
}

global.document = doc;
global.window = win;

Object.keys(window).forEach((key) => {
  if (!(key in global)) {
    global[key] = window[key];
  }
});


// var html = '<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <title>Cat Clicker</title></head><body> <ul id="cat-list"></ul> <div id="cat"> <h2 id="cat-name"></h2> <div id="cat-count"></div> <img id="cat-img" src="" alt="cute cat"> </div> <script src="js/app.js"></script></body></html>'; 
// jsdom.env(html, function(err, window) { 
//   global.window = window; 
// });

// last
// var jsdom = require("jsdom").jsdom;

// var html = '<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <title>Cat Clicker</title></head><body> <ul id="cat-list"></ul> <div id="cat"> <h2 id="cat-name"></h2> <div id="cat-count"></div> <img id="cat-img" src="" alt="cute cat"> </div> <script src="js/app.js"></script></body></html>'; 
// beforeEach(function(done) { 
//   jsdom.env(html, function(err, window) { 
//     global.window = window; 
//     console.log('it ran!');
//     done(); 
//   }); 
// });



// var jsdom = require("jsdom").jsdom;
// jsdom.env('<!DOCTYPE html><html lang="en"><head> <meta charset="UTF-8"> <title>Cat Clicker</title></head><body> <ul id="cat-list"></ul> <div id="cat"> <h2 id="cat-name"></h2> <div id="cat-count"></div> <img id="cat-img" src="" alt="cute cat"> </div> <script src="js/app.js"></script></body></html>', function(err, window) { 
//   global.window = window; 
//   global.document = window.document
// }
// ); 